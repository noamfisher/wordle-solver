import os
import platform

if "windows" in platform.system().lower():
    os.system("color")


from wordle_solver.consts import LEGAL_WORDS
from wordle_solver.base_wordle_solver import BaseWordleSolver, WordNotFoundError
from wordle_solver.auto_wordle_solver import AutoWordleSolver
from wordle_solver.manual_wordle_solver import ManualWordleSolver
