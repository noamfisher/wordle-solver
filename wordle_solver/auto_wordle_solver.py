import colorama

import wordle_solver


class AutoWordleSolver(wordle_solver.BaseWordleSolver):
    def __init__(self, *args, correct_word: str, **kwargs):
        super(AutoWordleSolver, self).__init__(*args, **kwargs)
        self.correct_word = correct_word

    def get_guess_result(self, guess: str) -> str:
        correct_word_letters = list(self.correct_word)

        guess_letters = list(guess)
        results = [""] * 5
        for index, letter in enumerate(guess_letters):
            correct_letter = correct_word_letters[index]
            if letter == correct_letter:
                results[index] = "G"
                guess_letters[index] = "-"
                correct_word_letters[index] = "-"

        for index, letter in enumerate(guess_letters):
            if letter == "-" or letter not in correct_word_letters:
                continue

            correct_letter_index = correct_word_letters.index(letter)
            results[index] = "Y"
            guess_letters[index] = "-"
            correct_word_letters[correct_letter_index] = "-"

        for index, letter in enumerate(guess_letters):
            if letter == "-":
                continue
            results[index] = "X"

        return "".join(results)

    def on_new_guess(self, guess: str):
        self.print(f"Trying {self.get_colored_text(guess, colorama.Fore.RED)}")


if __name__ == "__main__":
    solver = AutoWordleSolver(words=wordle_solver.LEGAL_WORDS, correct_word="arose")
    solver.run()
