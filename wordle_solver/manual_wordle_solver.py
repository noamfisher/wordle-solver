import re

import colorama

import wordle_solver


class ManualWordleSolver(wordle_solver.BaseWordleSolver):
    def get_guess_result(self, guess) -> str:
        valid = False
        first = True
        guess_results = None
        while not valid:
            if first:
                first = False
            else:
                self.print(f"WRONG INPUT {guess_results}", end=", ")

            self.print(
                f"Enter Guess Result from WORDLE: {self.get_colored_text('X', colorama.Fore.LIGHTBLACK_EX)}/{self.get_colored_text('Y', colorama.Fore.YELLOW)}/{self.get_colored_text('G', colorama.Fore.GREEN)} - for {self.get_colored_text('Grey', colorama.Fore.LIGHTBLACK_EX)}, {self.get_colored_text('Yellow', colorama.Fore.YELLOW)} or {self.get_colored_text('Green', colorama.Fore.GREEN)}: ",
                end="",
            )
            guess_results = input()
            valid = bool(re.match(r"^[XGY]{5}$", guess_results, flags=re.IGNORECASE))
        return guess_results.upper()

    def handle_word_not_found(self, e: wordle_solver.WordNotFoundError):
        self.print(repr(e))
        super(ManualWordleSolver, self).handle_word_not_found(e)

    def on_new_guess(self, guess: str):
        self.print(f"Try This: {self.get_colored_text(guess, colorama.Fore.RED)}")


if __name__ == "__main__":
    wordle_solver = ManualWordleSolver(wordle_solver.LEGAL_WORDS)
    wordle_solver.run()
