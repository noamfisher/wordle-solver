import abc
import re
import string
from typing import List, Dict, Optional

import colorama


class WordNotFoundError(Exception):
    pass


class BaseWordleSolver(abc.ABC):
    def __init__(self, words: List[str], first_guess: Optional[str] = None):
        self.words = words
        self.guesses_results = list()
        self.first_guess = first_guess

    def get_letters_scores(self) -> Dict[str, List[float]]:
        result = {letter: [0] * 5 for letter in string.ascii_lowercase}
        for word in self.words:
            for index, letter in enumerate(word):
                result[letter][index] += 1
        return result

    def get_word_score(self, word: str, letters_scores: Dict[str, List[float]]) -> float:
        letters_seen = set()
        total_score = 0
        for index, letter in enumerate(word):
            if letter in letters_seen:
                continue
            letters_seen.add(letter)
            total_score += letters_scores[letter][index]

        return total_score

    def get_guess_word(self) -> str:
        letters_scores = self.get_letters_scores()

        sorted_scored_words = sorted(
            self.words,
            key=lambda w: self.get_word_score(w, letters_scores),
            reverse=True,
        )
        if len(sorted_scored_words) == 0:
            raise WordNotFoundError("Can't find a word")
        return sorted_scored_words[0]

    def update_words_remove_letter(self, letter: str):
        self.words = list(filter(lambda w: letter not in w, self.words))

    def update_words_only_letter_in_index(self, letter: str, index: int):
        self.words = list(filter(lambda w: w[index] == letter, self.words))

    def update_words_only_letter_not_in_index(self, letter: str, index: int):
        self.words = list(filter(lambda w: w[index] != letter, self.words))

    def update_words_only_containing_letter(self, letter: str):
        self.words = list(filter(lambda w: letter in w, self.words))

    def update_words_letter_can_repeat_only_n_times(self, letter: str, n: int):
        self.words = list(filter(lambda w: len(re.findall(letter, w)) <= n, self.words))

    def run(self) -> int:
        tries = 0

        while True:
            tries += 1
            if tries == 1 and self.first_guess is not None:
                guess = self.first_guess
            else:
                try:
                    guess = self.get_guess_word()
                except WordNotFoundError as e:
                    self.handle_word_not_found(e)
                    break

            correct_word = self.run_one_round(guess)

            if correct_word is not None:
                self.on_correct_guess(correct_word, tries)
                break
        return tries

    def handle_word_not_found(self, e: WordNotFoundError):
        raise e

    def on_correct_guess(self, correct_word: str, tries: int):
        self.print(f"YAY, the word is {correct_word.upper()}")
        self.print_final_status()
        self.print(f"Took {tries} tries")

    def print_final_status(self):
        for guess_results in self.guesses_results:
            line = self.get_result_line(guess_results)
            self.print(line)

    def get_result_line(self, guess_results):
        squares = []
        for block in guess_results:
            if block == "X":
                color = colorama.Fore.LIGHTBLACK_EX
            elif block == "Y":
                color = colorama.Fore.YELLOW
            else:
                color = colorama.Fore.GREEN
            squares.append(self.get_colored_text("■", color))
        return f" ".join(squares)

    def run_one_round(self, guess: str) -> Optional[str]:
        self.on_new_guess(guess)
        guess_results = self.get_guess_result(guess)
        self.on_new_guess_result(guess_results)
        if guess_results == "GGGGG":
            return guess

        for index, guess_result in enumerate(guess_results):
            letter = guess[index]
            if guess_result == "X":
                count = len(re.findall(letter, guess))
                if count == 1:
                    self.update_words_remove_letter(letter)
                else:
                    self.update_words_only_letter_not_in_index(letter, index)
                    self.update_words_letter_can_repeat_only_n_times(letter, count)
            elif guess_result == "G":
                self.update_words_only_letter_in_index(letter, index)
            elif guess_result == "Y":
                self.update_words_only_letter_not_in_index(letter, index)
                self.update_words_only_containing_letter(letter)

    def on_new_guess_result(self, guess_results: str):
        self.print(self.get_result_line(guess_results))
        self.guesses_results.append(guess_results)

    def on_new_guess(self, guess: str):
        pass

    @abc.abstractmethod
    def get_guess_result(self, guess: str) -> str:
        raise NotImplementedError

    def print(self, *args, **kwargs):
        print(*args, **kwargs)

    def get_colored_text(self, text: str, color: colorama.Fore) -> str:
        return f"{color}{text}{colorama.Fore.RESET}"
