import os

import pytest

import wordle_solver


@pytest.mark.parametrize("word", [os.environ["WORDLE_WORD_TO_TRY"]])
def test(word: str):
    solver = wordle_solver.AutoWordleSolver(words=wordle_solver.LEGAL_WORDS, correct_word=word)
    solver.run()
