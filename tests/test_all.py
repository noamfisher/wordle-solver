import wordle_solver


class NonPrintingAutoWordleSolver(wordle_solver.AutoWordleSolver):
    def print(self, *args, **kwargs):
        pass


# @pytest.mark.parametrize("word", consts.LEGAL_WORDS)
# def test(word: str):
#     solver = NonPrintingAutoWordleSolver(words=consts.LEGAL_WORDS, correct_word=word)
#     solver.run()

solver = NonPrintingAutoWordleSolver(words=wordle_solver.LEGAL_WORDS, correct_word="phone")
first_guess = solver.get_guess_word()


tries = 0
for i, word in enumerate(wordle_solver.LEGAL_WORDS):
    if i % 400 == 0:
        print(i)
    solver = NonPrintingAutoWordleSolver(words=wordle_solver.LEGAL_WORDS, correct_word=word, first_guess=first_guess)
    tries += solver.run()

print(tries / len(wordle_solver.LEGAL_WORDS))
